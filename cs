#!/usr/bin/env python
import boto.ec2
import sys
import subprocess
from cfig import *

if len(sys.argv) > 1:
    name = sys.argv[1]
else:
    print "instance name is required"
    sys.exit(2)

# Config file used is /root/.aws/config
cfig = cfig()

conn = boto.ec2.connect_to_region(cfig.region, aws_access_key_id=cfig.access_key, aws_secret_access_key=cfig.secret_key)

reservations = conn.get_all_reservations()
for r in reservations:
   for i in r.instances:
        if 'Name' in i.tags and i.tags['Name'] == name:
            ip = i.private_ip_address
            try:
                subprocess.call(['ssh', ip])
            except KeyboardInterrupt:
                sys.exit(0)
