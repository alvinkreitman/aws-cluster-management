import ConfigParser
import os.path, sys

class cfig():
    def __init__(self):
        config = ConfigParser.RawConfigParser()

        # These are the config file.  Use local first if it exists. otherwise use the aws config that is in the /root/aws folder
        home = os.path.expanduser("~")
        aws_config =  home + '/.aws/config'
        local_config = os.getcwd() + '/aws_config'
        file_cred = home + '/.aws/credentials'


        aws_config_exists = os.path.isfile(aws_config)
        local_config_exists = os.path.isfile(local_config)

        if (not (local_config_exists or aws_config_exists)):
            sys.exit("ERROR - no config file found")

        file = local_config if local_config_exists else aws_config
        two_file = (file == aws_config and os.path.isfile(file_cred))

        try:
            if (two_file):
                config.read(file)
                self.region = config.get('default','region')
                config.read(file_cred)
                self.access_key = config.get('default','aws_access_key_id')
                self.secret_key = config.get('default','aws_secret_access_key')
            else:
                config.read(file)
                self.region = config.get('default','region')
                self.access_key = config.get('default','aws_access_key_id')
                self.secret_key = config.get('default','aws_secret_access_key')
        except:
            sys.exit('error - config file is missing or malformed.')
